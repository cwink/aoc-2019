#!/bin/bash

COMMAND="${1}"

BINARY_DIRECTORY='binary'

SOURCE_DIRECTORY='source'

INCLUDE_DIRECTORY='include'

DATA_DIRECTORY='data'

if [ "${CC}" == '' ]; then
  CC='clang++'
fi

if [ "${CC}" == 'clang++' ]; then
  CPPFLAGS="${CPPFLAGS} -Weverything -Wno-c++98-compat -pedantic-errors -std=c++17 -O2 -fno-exceptions -I${INCLUDE_DIRECTORY}"

  LDFLAGS="${LDFLAGS}"

  LIB="${LIB}"
fi

if [ "${CC}" == 'g++' ]; then
  CPPFLAGS="${CPPFLAGS} -Wall -Wextra -Wno-c++98-compat -pedantic-errors -std=c++17 -O2 -fno-exceptions -I${INCLUDE_DIRECTORY}"

  LDFLAGS="${LDFLAGS}"

  LIB="${LIB}"
fi

if [ "${COMMAND}" == '' ]; then
  echo 'Please supply one of the following commands to this script:' >&1 2>&1
  echo '    run     - Run the executables.' >&1 2>&1
  echo '    clean   - Clean the build.' >&1 2>&1
  echo '    build   - Build the source code.' >&1 2>&1
  echo '    format  - Format the source code.' >&1 2>&1
  echo '    tidy    - Tidy the source code.' >&1 2>&1
  echo '    all     - Clean, format, tidy, build, and run the code.' >&1 2>&1
  echo '    runner  - Clean, build, and run the code.' >&1 2>&1
  echo '    process - Format, and tidy the code.' >&1 2>&1
  echo '' >&1 2>&1

  exit 0
fi

if [ "$(command -v ${CC})" == '' ]; then
  echo "Could not find compiler '${CC}'." >&2 1>&2

  exit 1
fi

if [ "$(command -v clang-format)" == '' ]; then
  echo "Could not find clang-format. Please install it." >&2 1>&2

  exit 2
fi

if [ "$(command -v clang-tidy)" == '' ]; then
  echo "Could not find clang-tidy. Please install it." >&2 1>&2

  exit 3
fi

command_run() {
  for file in `find "${BINARY_DIRECTORY}" -name *.x | sort -u | cut -d'.' -f-1 | cut -d'/' -f2`; do
    echo "=============================== Running ${file}" >&1 2>&1

    time cat "${DATA_DIRECTORY}/${file}.txt" | "${BINARY_DIRECTORY}/${file}.x"

    if [ ${?} -ne 0 ]; then
      exit 4
    fi

    echo '' >&1 2>&1
  done
}

command_clean() {
  rm -rf "${BINARY_DIRECTORY}"
}

command_build() {
  mkdir -p "${BINARY_DIRECTORY}"

  for file in `find "${SOURCE_DIRECTORY}" -name *.cpp | sort -u | cut -d'.' -f-1 | cut -d'/' -f2`; do
    echo "=============================== Building ${file}" >&1 2>&1

    ${CC} ${CPPFLAGS} ${LDFLAGS} -o "${BINARY_DIRECTORY}/${file}.x" "${SOURCE_DIRECTORY}/${file}.cpp" ${LIB}

    if [ ${?} -ne 0 ]; then
      exit 5
    fi

    echo '' >&1 2>&1
  done
}

command_format() {
  for file in `find "${SOURCE_DIRECTORY}" -name *.cpp | sort -u | cut -d'.' -f-1 | cut -d'/' -f2`; do
    echo "=============================== Formatting ${file}" >&1 2>&1

    clang-format -i -style=file "${SOURCE_DIRECTORY}/${file}.cpp"

    if [ ${?} -ne 0 ]; then
      exit 6
    fi

    echo '' >&1 2>&1
  done

  for file in `find "${INCLUDE_DIRECTORY}" -name *.hpp | sort -u | cut -d'.' -f-1 | cut -d'/' -f2`; do
    echo "=============================== Formatting ${file}" >&1 2>&1

    clang-format -i -style=file "${INCLUDE_DIRECTORY}/${file}.hpp"

    if [ ${?} -ne 0 ]; then
      exit 7
    fi

    echo '' >&1 2>&1
  done
}

command_tidy() {
  for file in `find "${SOURCE_DIRECTORY}" -name *.cpp | sort -u | cut -d'.' -f-1 | cut -d'/' -f2`; do
    echo "=============================== Linting ${file}" >&1 2>&1

    clang-tidy "${SOURCE_DIRECTORY}/${file}.cpp" -- -I${INCLUDE_DIRECTORY}

    if [ ${?} -ne 0 ]; then
      exit 6
    fi

    echo '' >&1 2>&1
  done

  for file in `find "${INCLUDE_DIRECTORY}" -name *.hpp | sort -u | cut -d'.' -f-1 | cut -d'/' -f2`; do
    echo "=============================== Linting ${file}" >&1 2>&1

    clang-tidy "${INCLUDE_DIRECTORY}/${file}.hpp" -- -I${INCLUDE_DIRECTORY}

    if [ ${?} -ne 0 ]; then
      exit 7
    fi

    echo '' >&1 2>&1
  done
}

case "${COMMAND}" in
  'run')
    command_run

    ;;
  'clean')
    command_clean

    ;;
  'build')
    command_clean

    command_build

    ;;
  'all')
    command_clean

    command_format

    command_tidy

    command_build

    command_run

    ;;

  'runner')
    command_clean

    command_build

    command_run

    ;;

  'process')
    command_format

    command_tidy

    ;;

  'format')
    command_format

    ;;
  'tidy')
    command_tidy

    ;;
  *)
    echo "Invalid command '${COMMAND}' provided. Please provide a valid command." >&2 1>&2

    exit 6

    ;;
esac

exit 0
