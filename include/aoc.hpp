#pragma once // NOLINT

#include <algorithm>
#include <cstdarg>
#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <string>
#include <vector>

namespace exit_code {
constexpr auto success = EXIT_SUCCESS;

constexpr auto failure = EXIT_FAILURE;
} // namespace exit_code

namespace log {
enum class Error : std::uint8_t { none, null_format_argument, vfprintf_failed };

inline auto error(const char *const format, ...) noexcept -> Error { // NOLINT
  if (format == nullptr) {
    return Error::null_format_argument;
  }

  std::va_list arguments;

  va_start(arguments, format); // NOLINT

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wformat-nonliteral"
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wformat-nonliteral"
  if (std::vfprintf(stderr, format, arguments) < 0) { // NOLINT
    va_end(arguments);                                // NOLINT

    return Error::vfprintf_failed;
  }
#pragma clang diagnostic pop
#pragma GCC diagnostic pop

  fprintf(stderr, "\n"); // NOLINT

  va_end(arguments); // NOLINT

  return Error::none;
}

inline auto info(const char *const format, ...) noexcept -> Error { // NOLINT
  if (format == nullptr) {
    return Error::null_format_argument;
  }

  std::va_list arguments;

  va_start(arguments, format); // NOLINT

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wformat-nonliteral"
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wformat-nonliteral"
  if (std::vfprintf(stdout, format, arguments) < 0) { // NOLINT
    va_end(arguments);                                // NOLINT

    return Error::vfprintf_failed;
  }
#pragma clang diagnostic pop
#pragma GCC diagnostic pop

  fprintf(stdout, "\n"); // NOLINT

  va_end(arguments); // NOLINT

  return Error::none;
}

inline auto print(const char *const format, ...) noexcept -> Error { // NOLINT
  if (format == nullptr) {
    return Error::null_format_argument;
  }

  std::va_list arguments;

  va_start(arguments, format); // NOLINT

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wformat-nonliteral"
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wformat-nonliteral"
  if (std::vfprintf(stdout, format, arguments) < 0) { // NOLINT
    va_end(arguments);                                // NOLINT

    return Error::vfprintf_failed;
  }
#pragma clang diagnostic pop
#pragma GCC diagnostic pop

  va_end(arguments); // NOLINT

  return Error::none;
}
} // namespace log

namespace pipe {
enum class Result : std::uint8_t { cont, brk, success, fail };

template <typename Lambda>
inline auto read_stdin(const Lambda &lambda, const char delimiter = '\0') noexcept -> Result {
  if (delimiter == '\0') {
    for (auto line = std::string(); std::getline(std::cin, line);) {
      const auto result = lambda(line);

      if (result != Result::cont) {
        return result;
      }
    }
  } else {
    for (auto line = std::string(); std::getline(std::cin, line, delimiter);) {
      const auto result = lambda(line);

      if (result != Result::cont) {
        return result;
      }
    }
  }

  return Result::success;
}
} // namespace pipe

namespace str {
inline auto split(const std::string &str, const char delimiter) noexcept -> std::vector<std::string> {
  auto vec = std::vector<std::string>();

  for (auto it = std::cbegin(str), jt = std::find(it, std::cend(str), delimiter); it < std::cend(str);
       it = jt + 1, jt = std::find(it, std::cend(str), delimiter)) {
    vec.emplace_back(std::string(it, jt));
  }

  return vec;
}
} // namespace str
