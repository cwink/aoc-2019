#include <aoc.hpp>

#include <utility>
#include <vector>

namespace {
struct Point final {
  std::int32_t x = 0;

  std::int32_t y = 0;

  std::int32_t magnitude = 0;
};

struct Line final {
  Point a = {0, 0, 0};

  Point b = {0, 0, 0};
};

enum class Direction : std::uint8_t { neither, horizontal, vertical };

inline auto direction(const Line &line) noexcept -> Direction {
  if (line.a.x == line.b.x) {
    return Direction::vertical;
  }

  if (line.a.y == line.b.y) {
    return Direction::horizontal;
  }

  return Direction::neither;
}

inline auto in_middle(const int32_t x, const int32_t y, const int32_t z) noexcept -> bool {
  return std::min(x, y) <= z && z <= std::max(x, y);
}

inline auto intersects(const Line &line_1, const Line &line_2) noexcept -> bool {
  return (direction(line_1) == Direction::horizontal && direction(line_2) == Direction::vertical &&
          in_middle(line_1.a.x, line_1.b.x, line_2.a.x) && in_middle(line_2.a.y, line_2.b.y, line_1.a.y)) ||
         (direction(line_1) == Direction::vertical && direction(line_2) == Direction::horizontal &&
          in_middle(line_1.a.y, line_1.b.y, line_2.a.y) && in_middle(line_2.a.x, line_2.b.x, line_1.a.x));
}
} // namespace

auto main([[maybe_unused]] int argc, [[maybe_unused]] char *argv[]) noexcept -> int {
  auto wire_1 = std::vector<Point>();

  auto result = pipe::read_stdin([&wire_1](const auto &line) -> pipe::Result {
    const auto splitted = str::split(line, ',');

    auto previous = Point{0, 0};

    auto current = Point{0, 0};

    for (const auto &item : splitted) {
      const auto magnitude = std::stoi(std::string(std::cbegin(item) + 1, std::cend(item)));

      current = previous;

      switch (item[0]) {
      case 'R':
        current.x += magnitude;

        break;
      case 'L':
        current.x -= magnitude;

        break;
      case 'U':
        current.y += magnitude;

        break;
      case 'D':
        current.y -= magnitude;

        break;
      default:
        log::error("ERROR: Unknown direction '%c' found.", item[0]); // NOLINT

        return pipe::Result::fail;
      }

      current.magnitude += magnitude;

      wire_1.emplace_back(current);

      previous = current;
    }

    return pipe::Result::success;
  });

  if (result == pipe::Result::fail) {
    return exit_code::failure;
  }

  auto final_steps = -1;

  result = pipe::read_stdin([&wire_1, &final_steps](const auto &line) -> pipe::Result {
    const auto splitted = str::split(line, ',');

    auto previous = Point{0, 0};

    auto current = Point{0, 0};

    for (const auto &item : splitted) {
      const auto magnitude = std::stoi(std::string(std::cbegin(item) + 1, std::cend(item)));

      current = previous;

      switch (item[0]) {
      case 'R':
        current.x += magnitude;

        break;
      case 'L':
        current.x -= magnitude;

        break;
      case 'U':
        current.y += magnitude;

        break;
      case 'D':
        current.y -= magnitude;

        break;
      default:
        log::error("ERROR: Unknown direction '%c' found.", item[0]); // NOLINT

        return pipe::Result::fail;
      }

      current.magnitude += magnitude;

      for (auto it = std::cbegin(wire_1) + 1; it < std::cend(wire_1); ++it) {
        const auto line_1 = Line{*(it - 1), *it};

        const auto line_2 = Line{previous, current};

        if (intersects(line_1, line_2)) {
          auto point = Point{line_2.a.x, line_1.a.y};

          const auto steps_1 = (*(it - 1)).magnitude +
                               std::abs(std::max(point.x, (*(it - 1)).x) - std::min(point.x, (*(it - 1)).x)) +
                               std::abs(std::max(point.y, (*(it - 1)).y) - std::min(point.x, (*(it - 1)).y));

          const auto steps_2 = previous.magnitude +
                               std::abs(std::max(point.x, previous.x) - std::min(point.x, previous.x)) +
                               std::abs(std::max(point.y, previous.y) - std::min(point.x, previous.y));

          const auto steps = steps_1 + steps_2;

          if (final_steps == -1 || steps < final_steps) {
            final_steps = steps;
          }
        }
      }

      previous = current;
    }

    return pipe::Result::success;
  });

  if (result == pipe::Result::fail) {
    return exit_code::failure;
  }

  if (final_steps == -1) {
    log::error("ERROR: Did not find a distance value."); // NOLINT

    return exit_code::failure;
  }

  log::info("%d", final_steps); // NOLINT

  return exit_code::success;
}
