#include <aoc.hpp>

#include <array>

namespace {
constexpr auto max_array_size = 6;

constexpr auto number_divisor = 10;

constexpr auto max_number_size = 999999;

constexpr auto min_number_size = 100000;

inline auto number_to_array(const std::int32_t number) noexcept -> std::array<std::int32_t, max_array_size> {
  auto result = number;

  const auto a = result % number_divisor;

  result /= number_divisor;

  const auto b = result % number_divisor;

  result /= number_divisor;

  const auto c = result % number_divisor;

  result /= number_divisor;

  const auto d = result % number_divisor;

  result /= number_divisor;

  const auto e = result % number_divisor;

  result /= number_divisor;

  const auto f = result % number_divisor;

  return std::array<std::int32_t, max_array_size>{f, e, d, c, b, a};
}

inline auto check_ascending(const std::array<std::int32_t, max_array_size> &arr) noexcept -> bool {
  return arr[0] <= arr[1] && arr[1] <= arr[2] && arr[2] <= arr[3] && arr[3] <= arr[4] && arr[4] <= arr[5]; // NOLINT
}

inline auto check_double(const std::array<std::int32_t, max_array_size> &arr) noexcept -> bool {
  return arr[0] == arr[1] || arr[1] == arr[2] || arr[2] == arr[3] || arr[3] == arr[4] || arr[4] == arr[5]; // NOLINT
}

inline auto validate_number(const std::int32_t number) noexcept -> bool {
  if (number < min_number_size || number > max_number_size) {
    return false;
  }

  const auto arr = number_to_array(number);

  if (!check_ascending(arr)) {
    return false;
  }

  if (!check_double(arr)) {
    return false;
  }

  return true;
}
} // namespace

auto main([[maybe_unused]] int argc, [[maybe_unused]] char *argv[]) noexcept -> int {
  auto start_number = 0;

  auto end_number = 0;

  pipe::read_stdin([&start_number, &end_number](const auto &line) -> pipe::Result {
    const auto splitted = str::split(line, '-');

    start_number = std::stoi(splitted[0]);

    end_number = std::stoi(splitted[1]);

    return pipe::Result::success;
  });

  auto password_count = 0;

  for (auto number = start_number; number <= end_number; ++number) {
    if (validate_number(number)) {
      ++password_count;
    }
  }

  log::info("%d", password_count); // NOLINT

  return exit_code::success;
}
