#include <aoc.hpp>

namespace {
constexpr auto calculate_fuel_divisor = 3.0F;

constexpr auto calculate_fuel_subtractor = 2;

inline auto calculate_fuel(const std::int32_t mass) noexcept -> std::int32_t {
  return static_cast<std::int32_t>(static_cast<float>(mass) / calculate_fuel_divisor) - calculate_fuel_subtractor;
}
} // namespace

auto main([[maybe_unused]] int argc, [[maybe_unused]] char *argv[]) noexcept -> int {
  auto sum = static_cast<std::int32_t>(0);

  pipe::read_stdin([&sum](const auto &line) -> pipe::Result {
    sum += calculate_fuel(std::stoi(line));

    return pipe::Result::cont;
  });

  log::info("%d", sum); // NOLINT

  return exit_code::success;
}
