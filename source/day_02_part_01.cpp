#include <aoc.hpp>

namespace {
constexpr auto given_noun = 12;

constexpr auto given_verb = 2;
} // namespace

namespace opcodes {
constexpr auto halt = 99;

constexpr auto add = 1;

constexpr auto multiply = 2;
} // namespace opcodes

auto main([[maybe_unused]] int argc, [[maybe_unused]] char *argv[]) noexcept -> int {
  auto code = std::vector<std::int32_t>();

  pipe::read_stdin(
      [&code](const auto &line) -> pipe::Result {
        code.emplace_back(std::stoi(line));

        return pipe::Result::cont;
      },
      ',');

  code[1] = given_noun;

  code[2] = given_verb;

  for (auto it = std::begin(code); it != std::end(code) && *it != opcodes::halt; ++it) {
    const auto destination_code = static_cast<std::size_t>(*(it + 3));

    const auto first_paramter = static_cast<std::size_t>(*(it + 1));

    const auto second_paramter = static_cast<std::size_t>(*(it + 2));

    switch (*it) {
    case opcodes::add:
      code[destination_code] = code[first_paramter] + code[second_paramter];

      it += 3;

      break;
    case opcodes::multiply:
      code[destination_code] = code[first_paramter] * code[second_paramter];

      it += 3;

      break;
    default:
      log::error("ERROR: Invalid opcode '%d' found.", *it); // NOLINT

      return exit_code::failure;
    }
  }

  log::info("%d", code[0]); // NOLINT

  return exit_code::success;
}
