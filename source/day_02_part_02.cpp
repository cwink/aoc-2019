#include <aoc.hpp>

namespace {
constexpr auto magic_multiplier = 100;
} // namespace

namespace opcodes {
constexpr auto halt = 99;

constexpr auto add = 1;

constexpr auto multiply = 2;
} // namespace opcodes

auto main([[maybe_unused]] int argc, [[maybe_unused]] char *argv[]) noexcept -> int {
  auto initial_code = std::vector<std::int32_t>();

  pipe::read_stdin([&initial_code](const auto &line) -> pipe::Result {
    const auto splitted = str::split(line, ',');

    std::transform(std::cbegin(splitted), std::cend(splitted), std::back_inserter(initial_code),
                   [](const auto &value) -> std::int32_t { return std::stoi(value); });

    return pipe::Result::success;
  });

  auto parameters = std::vector<std::int32_t>();

  pipe::read_stdin([&parameters](const auto &line) -> pipe::Result {
    parameters.emplace_back(std::stoi(line));

    return pipe::Result::cont;
  });

  std::int32_t result = -1;

  for (auto noun = parameters[0]; noun <= parameters[1]; ++noun) {
    for (auto verb = parameters[0]; verb <= parameters[1]; ++verb) {
      auto code = initial_code;

      code[1] = noun;

      code[2] = verb;

      for (auto it = std::begin(code); it != std::end(code) && *it != opcodes::halt; ++it) {
        const auto destination_code = static_cast<std::size_t>(*(it + 3));

        const auto first_paramter = static_cast<std::size_t>(*(it + 1));

        const auto second_paramter = static_cast<std::size_t>(*(it + 2));

        switch (*it) {
        case opcodes::add:
          code[destination_code] = code[first_paramter] + code[second_paramter];

          it += 3;

          break;
        case opcodes::multiply:
          code[destination_code] = code[first_paramter] * code[second_paramter];

          it += 3;

          break;
        default:
          log::error("ERROR: Invalid opcode '%d' found.", *it); // NOLINT

          return exit_code::failure;
        }
      }

      if (code[0] == parameters[2]) {
        result = magic_multiplier * noun + verb;
      }
    }
  }

  if (result == -1) {
    log::error("ERROR: No proper noun/verb combo resulted in '%d'.", parameters[2]); // NOLINT
  }

  log::info("%d", result); // NOLINT

  return exit_code::success;
}
